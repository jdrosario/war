
var map;


function initMap() 
{
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:  38.659784, lng:  -9.202765},
		zoom: 3
	});
}

function geocodeAddress(geocoder, resultsMap, address) {
	
		geocoder.geocode({'address': address.propertyMap.user_address + ", " + address.propertyMap.user_locality}, function(results, status) {
		if (status === 'OK') {
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
			
			var contentString = '<div id="content">'+
			'<h1 id="title">User Information</h1>'+
		    '<p><b>Username: </b>'+ address.key.name +
			'<p><b>Address: </b>'+ address.propertyMap.user_address +
			'<p><b>Locality: </b>'+ address.propertyMap.user_locality;
		   

		var infowindow = new google.maps.InfoWindow({content: contentString});

		marker.addListener('click', function() {
		    infowindow.open(map, marker);
		});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
	
};

function getLoc() {
	$.ajax({
		type: "POST",
		url: "http://active-tangent-159416.appspot.com/rest/operation/getList/" + localStorage.getItem('username'),
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				initMap();
				var geocoder = new google.maps.Geocoder();
				for(var i = 0; i<response.length; i++) 
					geocodeAddress(geocoder, map, response[i]);
			}
			else {
				alert("No response");
				window.location.href = "http://active-tangent-159416.appspot.com/index.html";
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
			window.location.href = "http://active-tangent-159416.appspot.com/login.html";
		},
		data: JSON.stringify(JSON.parse("{\"tokenID\":\"" + localStorage.getItem('tokenID') + "\"}"))
	});

}; 

function listUsers() {
    $.ajax({
        type: "POST",
        url: "http://active-tangent-159416.appspot.com/rest/operation/getList/" + localStorage.getItem('username'),
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
            	console.log(response);
                if(response != null)
                		for(var i=0; i < response.length; i++)
                			document.getElementById('demo').innerHTML += response[i].key.name + "<br />";
            }
            else {
                alert("No response");
                window.location.href = "http://active-tangent-159416.appspot.com/index.html";
            }
        },
        error: function(response) {
        	alert("Error: "+ response.status);
        },
        data: JSON.stringify(JSON.parse("{\"tokenID\":\"" + localStorage.getItem('tokenID') + "\"}"))
    });
};





