captureData = function(event) {
    var data = $('form[name="form-register"]').jsonify();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "http://active-tangent-159416.appspot.com/rest/register",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                alert("You are now registered!");
                window.location.href = "http://active-tangent-159416.appspot.com/login.html";
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(data)
    });

    event.preventDefault();
};

window.onload = function() {
    var frms = $('form[name="form-register"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}
